// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import vueHeadful from 'vue-headful'

const SocialSharing = require('vue-social-sharing')

Vue.use(SocialSharing)
Vue.use(ElementUI)
Vue.component('vue-headful', vueHeadful)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  template: '<App/>'
})
